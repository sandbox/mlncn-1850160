<?php
/**
 * @file
 *  Manage Catalog - Catalog Mgr Admin forms to customize the Catalog for a particular Catalog instance
 */

/**
 * Define
 */
define('LIBCAT_BOOK_LOAD_DIRECTORY', 'public://libcat_book_load/');

/**
 * Implements hook_menu().
 * @return string
 */
function libcat_book_load_menu() {
  $items = array();

  $items['admin/libcat/book_load'] = array(
    'title' => t('Bulk book load'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('libcat_book_load_form'),
    'access arguments' => array(LIBCAT_ADMIN_MANAGE_COLLECTION),
    'weight' => 50,
  );
  return $items;
}

/**
 * Define the form for the  bulk book upload.
 */
function libcat_book_load_form() {
  $form = array();

  $form['intro'] = array('#markup' => t('Use this form for bulk upload of books, for example an initial import of books to create a new catalog, or if you acquire a company with additional books to be loaded into your catalog.'));

  $data_template = base_path() . drupal_get_path('module', 'libcat_book_load') . '/BookLoadTemplate.csv';
  $form['instructions'] = array(
    '#prefix' => '<div class="instructions">',
    '#markup' => '<p>' . t('The file of book data should be a comma-separated value (CSV) file or a tab-separated value file (with the extension .tsv or .txt) with the columns in this order: Collection (id number or name, or e-mail address of owner for a personal collection), ISBN (optional, ISBN10 or ISBN13; if provided, title, authors, description and other book values will be fetched for you), Barcode (optional), Title, Authors, Publication date, Publisher (optional), Description (optional), Keywords (optonal), Dewey Decimal number (optional), Library of Congress number (optional)') . '</p>' .
    '<p>' . t('Download the <a href="!data_template">Bulk Book Upload Template File</a>', array("!data_template" => $data_template)) . '</p>',
    '#suffix' => '</div>',
  );

  $form['file'] = array(
    '#title' => t('Book bulk upload file'),
    '#type' => 'file',
    '#description' => ($max_size = parse_size(ini_get('upload_max_filesize'))) ? t('Due to server restrictions, the <strong>maximum upload file size is !max_size</strong>.', array('!max_size' => format_size($max_size))) : '',
    '#prefix' => '',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Commence import'),
  );

  return $form;
}

/**
 * Validate the file upload.
 *
 * It must be a csv, tsv, or txt and we must save it to our import directory.
 *
 * Further ensure the data looks something like we expect it to.
 */
function libcat_book_load_form_validate(&$form, &$form_state) {
  $validators = array(
    'file_validate_extensions' => array('csv tsv txt'),
  );
  $file = file_save_upload('file', $validators);
  if ($file) {
    $directory = LIBCAT_BOOK_LOAD_DIRECTORY;
    file_prepare_directory($directory, FILE_CREATE_DIRECTORY);
    $destination = LIBCAT_BOOK_LOAD_DIRECTORY . $file->filename;
    $file = file_move($file, $destination, FILE_EXISTS_REPLACE);
    if ($file) {
      $form_state['storage']['file'] = $file;
    }
    else {
      form_set_error('file', t('Unable to move the uploaded file to !dest', array('!dest' => $destination)));
      return;
    }
  }
  else {
    // Validation error messages already set by file_save_upload();
    return;
  }

  $delimiter = ',';
  $ext = strtolower(substr($file->filename, -4));
  if ($ext == '.tsv' || $ext == '.txt') {
    $delimiter = "\t";
  }
  $form_state['storage']['delimiter'] = $delimiter;
  ini_set('auto_detect_line_endings', TRUE);

  // fopen works just as well as: $contents = file_get_contents($file->uri);
  $handle = fopen($file->uri, 'r');
  if ($handle) {
    $line_count = 1;
    $first = TRUE;
    $line = fgetcsv($handle, 4096, $delimiter);
    if ($line) {
      /**
       * Validate the uploaded CSV here.
       *
       * Each $line is a row, columns are indexes starting at zero.
       */
      $collection_header = strtolower($line[0]);
      if ($collection_header != 'collection') {
        form_set_error('file', t('Sorry, this file does not match the expected format.  Please review the instructions for the Book Load Template file and make sure the header line with Collection, ISBN, etc is included.'));
      }
    }
    fclose($handle);
  }
  else {
    form_set_error('file', t('Unable to read uploaded file !filepath',
      array('!filepath' => $file)));
  }
  ini_set('auto_detect_line_endings', FALSE);
}

/**
 * Handle form submission. Read the CSV into a set of batch operations
 * and fire them off.
 */
function libcat_book_load_form_submit(&$form, &$form_state) {
  $file = $form_state['storage']['file'];
  $delimiter = $form_state['storage']['delimiter'];
  libcat_book_load_file($file, $delimiter);
}

/**
 * Batch import a CSV or TSV file of books.
 *
 * @param object $file
 * @param string $delemiter
 *   A comma (",") or tab ("/t").
 */
function libcat_book_load_file($file, $delimiter) {
  $batch = array(
    'title' => t('Importing books...'),
    'operations' => array(),
    'init_message' => t('Commencing'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('An error occurred during processing'),
    'finished' => 'libcat_book_load_import_finished',
  );

  ini_set('auto_detect_line_endings', TRUE);
  $handle = fopen($file->uri, 'r');
  if ($handle) {
    $batch['operations'][] = array(
      'libcat_book_load_remember_filename',
      array($file->filename),
    );
    $line_count = 0;
    $line = fgetcsv($handle, 4096, $delimiter);
    while ($line = fgetcsv($handle, 4096, $delimiter)) {
      // Ignore comment lines.
      if ($line[0][0] == '#') {
        continue;
      }
      $line_count++;
      // First line should have column headers.
      if ($line_count == 1) {
        continue;
      }

      /**
       * we use base64_encode to ensure we don't overload the batch
       * processor by stuffing complex objects into it
       */
      $batch['operations'][] = array(
        'libcat_book_load_import_line',
        array(array_map('base64_encode', $line)),
      );
    }
    fclose($handle);
  }
  ini_set('auto_detect_line_endings', FALSE);
  batch_set($batch);
}

/**
 * Helper batch function to remember the filename to be used in results report.
 */
function libcat_book_load_remember_filename($filename, &$context) {
  $context['results']['filename'] = $filename['filename'];
}

/**
 * Handle batch completion; write out post-import reports.
 */
function libcat_book_load_import_finished($success, $results, $operations) {
  if (!empty($results['feedback'])) {
    // file_prepare_directory() cannot be passed a constant, so assign it here.
    $directory = LIBCAT_BOOK_LOAD_DIRECTORY;
    file_prepare_directory($directory, FILE_CREATE_DIRECTORY);
    $form_state['storage']['file'] = $file;
    $filename = substr($results['filename'], -4);
    $report_filename = 'bookloadreport-' . REQUEST_TIME . '-' . $filename . '.csv';
    $report_filepath = LIBCAT_BOOK_LOAD_DIRECTORY . $report_filename;
    $targs = array(
      '!report_url' => l(check_plain($report_filename), file_create_url($report_filepath)),
      '%report_filename' => $report_filename,
      '%report_filepath' => $report_filepath,
    );
    if ($handle = fopen($report_filepath, 'w+')) {
      // Write CSV File header.
      $header = array('Book Link', 'AmazonInfo', 'Collection', 'ISBN', 'Barcode', 'Title', 'Original Title', 'Authors', 'PubDate', 'Publisher', 'Keywords', 'DeweyDecimal', 'LCC', 'Description');
      fputcsv($handle, $header);
      // Write CSV import results
      foreach ($results['feedback'] as $row) {
        fputcsv($handle, $row);
      }
      fclose($handle);
      drupal_set_message(t('Results of import. You may download a CSV of these results: !report_url', $targs));

      // Record file in the database as temporary file for future cleanup.
      $file = new stdClass();
      $file->filename = $report_filename;
      $file->filepath = $report_filepath;
      $file->filemime = file_get_mimetype($file->filename);
      $file->filesize = filesize($report_filepath);
      $file->uid = $GLOBALS['user']->uid;
      $file->status = ~FILE_STATUS_PERMANENT;
      $file->timestamp = REQUEST_TIME;

      drupal_write_record('files', $file);
    }
    else {
      drupal_set_message(t('Unable to write import results feedback CSV to %report_filepath', $targs), 'error');
    }
  }

  return t('The CSV import has completed.');
}

/**
 * Process a single line.
 */
function libcat_book_load_import_line($line, &$context) {
  $book = NULL;
  $amazon_success = FALSE;

  $context['results']['rows_imported']++;

  $line = array_map('base64_decode', $line);

  drupal_set_message(t('Importing row !c [%y columns] :: %x',
    array( '!c' => $context['results']['rows_imported'],
    '%y' => count($line),
    '%x' => implode('|',$line))
    )
  );
  //  Fields:  0: Collection	1: ISBN	 2: Barcode	3: Title	4: Authors 5: PubDate	6: Publisher	7: Description	8: Keywords	9: DeweyDecimal	10: LibraryCongressNumber
  $collection_identifier = $line[0];
  $book_item = array();
  $book_item['isbn'] = $line[1];
  $book_item['barcode'] = $line[2];
  $book_item['title'] = $line[3];
  $book_item['author'] = $line[4];
  $book_item['publicationdate'] = $line[5];
  $book_item['publisher'] = $line[6];
  $book_item['description'] = $line[7];
  $book_item['keywords'] = $line[8];
  $book_item['deweydecimal'] = $line[9];
  $book_item['locnum'] = $line[10];

  $collection = libcat_book_load_get_collection($collection_identifier);

  // If a collection was not loaded, we need to create it.
  if (!$collection) {
    $account = libcat_util_load_or_create_account($collection_identifier);
    $message = t('New collection @collection created for !modifier account with e-mail address !mail.', array('!modifier' => $modifier, '@collection' => $collection_identifier, '!mail' => $account->mail));
    drupal_set_message($message);
    $collection = libcat_collection_create(LIBCAT_COLLECTION_PERSONAL, $account, $collection_identifier);
  }

  // Load book with values from Amazon if ISBN exists.
  if ($isbn) {
    $book = libcat_collection_create_book_by_isbn($isbn, $collection->collection_id);
    // NOTE: As this may not work, we must still test $book variable for value.
  }

  // If no book we have to save the book, do the owning relation ourselves, etc.
  if (!$book) {
    // Parse Authors into Book variable
    $author = explode(';', $book_item['author']);
    foreach ($authors as $author) {
      $book_item['author'][] = $author;
    }
    // Explode Keywords and add to Book variable
    if ($book_item['$keywords']) {
      $keywords = explode(';', $book_item['keywords']);
      foreach ($keywords as $keyword) {
        $book['Authors'][] = $keyword;
      }
    }

    $book = libcat_book_prepare_object($book_item);
    libcat_collection_create_book_and_associate($book, $collection->collection_id);
  }
  // Otherwise, we are ignoring a bunch of data and should note differences.
  else {
    $amazon_success = TRUE;

  }

  if ($barcode) {
    // Check if barcode is unique.
//    $barcode_check = libcat_book_barcode_exists($barcode, $nid);
//    if ($barcode_check) {
//      $book_node = node_load($barcode_check);
//      $book_link = l($book_node->title, 'node/' . $book_node->nid);
//      drupal_set_message(t('Barcode :barcode for ISBN :isbn already used by the following book, :book_link. Please change the existing or new item barcode to complete adding book to catalog.', array(':barcode' => $barcode, ':book_link' => $book_link, ':isbn' => $isbn)), 'warning');
//    }
  }

  // Fields:  0: Owner	1: ISBN	 2: Barcode	3: Title	4: Authors 5: PubDate	6: Publisher	7: Description	8: Keywords	9: DeweyDecimal	10: LibraryCongressNumber

  $booklink = ($book) ? url('node/'. $book->nid) : '';

  // Feedback info
  // @TODO set the things not labeled 'original' to come from $book object.
  $feedback = array(
    $book_link,
    $amazon_success, // 1 if ISBN from Amazon was successful and 0 if not.
    $collection_identifier,
    $book_item['isbn'],
    $book_item['barcode'],
    $book->title,
    $book_item['title'],
    $authors,
    $book_item['publicationdate'],
    $book_item['publisher'],
    $book_item['keywords'],
    $book_item['DeweyDecimal'],
    $book_item['LCC'],
    preg_replace('/\n/', ' ', $book_item['description']),
  );

  $context['results']['feedback'][] = $feedback;

  // Show progress as batch operation proceeds.
  $context['message'] = t('Importing Book #!count  Owner: %owner Title: %title', array('%title' => $line[3], '%owner' => $collection_name, '!count' => $context['results']['rows_imported']));

}

/**
 * Given collection ID, collection name, OR e-mail address, return a collection.
 */
function libcat_book_load_get_collection($collection_identifier) {
  if (is_numeric($collection_identifier)) {
    $collection = libcat_collection_load($collection_identifier);
    return $collection;
  }
  elseif (is_string($collection_identifier)) {
    $collection = libcat_collection_load_by_name($collection_identifier);
    if ($collection) {
      return $collection;
    }
    // Last resort: we'll see if the string is an e-mail address of a user.
    $account = user_load_by_mail($collection_identifier);
    if ($account) {
      // First try to load an institutional collection owned by that user.
      $collection = libcat_collection_load_by_type_uid(LIBCAT_COLLECTION_INSTITUTIONAL, $account->uid);
      if ($collection) {
        return $collection;
      }
      else {
        // If that didn't work, try to load a personal collection of that user.
        $collection = libcat_collection_load_by_type_uid(LIBCAT_COLLECTION_PERSONAL, $account->uid);
        if ($collection) {
          return $collection;
        }
      }
    }
  }
  // We could not interpret input as a collection no matter how hard we tried.
  return FALSE;
}