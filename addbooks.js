//
// Add Books Form Javascript functions
//
// Enter ISBN number and have the rest of the book info filled in on the form
//
// Keep 'enter' key from submitting form
//
// http://www.libtbx.com/sites/all/scripts/amazon_lookup.py?addBook=1&asin=0971176760
//

(function($, Drupal) {
  Drupal.behaviors.libcatBookAdd = {
    attach: function (context, settings) {
      $('#edit-isbn', context).blur(function() {
        var isbn = $('#edit-isbn').val();
        // Initialize the form fields
        $('#edit-title').val('');
        $('#edit-libcat-authors-und-0-value').val('');
        // Get Amazon book info based on ISBN lookup
        $.get(settings.libcatUtilAmazonQuery, {
            addBook: 1,
            asin: isbn
        },
        function(xml) {
            $(xml).find('Book').each(function() {
                var title = $(this).find('Title').text();
                var author = $(this).find('AuthorList').text();
								// Attempt to concatenate multiple authors together into one field
								// $(this).find('Author').each(function(){
									// var author = author + "; " + $(this).find('Author').text();
								// });
                var isbn10 = $(this).find('ISBN10').text();
                var isbn13 = $(this).find('ISBN13').text();
                var dewey = $(this).find('Dewey').text();
                var publisher = $(this).find('Publisher').text();
                var pubdate = $(this).find('PublicationDate').text();
                var pubyear = pubdate.match(/\d{4,4}/);
                var amazonlink = $(this).find('AmazonLink').text();
                var coverimagelink = $(this).find('CoverImageLink').text();
                var description = $(this).find('Description').text();
                var keywords = $(this).find('SubjectList').text();

                $('#edit-title').val(title);
                $('#edit-body-und-0-value').val(description);
                $('#edit-libcat-pubdate-und-0-value-date').val(pubdate);
                $('#edit-libcat-publisher-und-0-value').val(publisher);
                $('#edit-libcat-isbn10-und-0-value').val(isbn10);
                $('#edit-libcat-isbn13-und-0-value').val(isbn13);
                $('#edit-libcat-item-link-und-0-url').val(amazonlink);
                $('#edit-libcat-authors-und-0-value').val(author);
                $('#edit-libcat-cover-und-0-filefield-remote-url').val(coverimagelink);
            });
        });
      })
    }
  }
})(jQuery, Drupal);

/*
    //capture the return key - only allow Submit button to work for submitting form - JQuery version from
		$("#addbooks-inputform").bind("keypress", function(e) {
		  if (e.keyCode == 13) return false;
		});
*/
